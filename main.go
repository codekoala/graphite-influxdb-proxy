package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	PAYLOAD_RE    = regexp.MustCompile(`^([^\.]+)\.([^\.]+)\.([^\.]+)(?:\.([^\s]+))?\s+([\d\.]+)\s+(\d+)$`)
	QUEUE_TIMEOUT time.Duration

	debug        = flag.Bool("debug", false, "Log more information")
	bind         = flag.String("bind", ":2003", "Listen for graphite data on this port")
	maxQueueSize = flag.Int("queue", 200, "Queue up to N metrics before sending to InfluxDB")
	queueTimeout = flag.Int("timeout", 5, "Number of seconds to wait before sending any outstanding metrics in the queue")

	influxSsl  = flag.Bool("ssl", false, "Whether to use https for InfluxDB")
	influxHost = flag.String("host", "localhost", "InfluxDB hostname or IP")
	influxPort = flag.Int("port", 8086, "InfluxDB API port")
	influxDb   = flag.String("db", "graphite", "InfluxDB database name")
	influxUser = flag.String("user", "", "InfluxDB username")
	influxPass = flag.String("pass", "", "InfluxDB password")
	influxUrl  string
)

func init() {
	flag.Parse()

	QUEUE_TIMEOUT = time.Duration(*queueTimeout) * time.Second
}

func main() {
	log.Println("Awaiting metrics:", *bind)
	sock, err := net.Listen("tcp", *bind)
	if err != nil {
		log.Fatal(err)
	}
	defer sock.Close()

	influxUrl = BuildInfluxUrl()
	for {
		conn, err := sock.Accept()
		if err != nil {
			log.Println("Error accepting connection: " + err.Error())
			continue
		}

		go ProcessMetrics(conn)
	}
}

func BuildInfluxUrl() string {
	proto := "http"
	if *influxSsl {
		proto = "https"
	}
	url := fmt.Sprintf("%s://%s:%d/write?db=%s&precision=s", proto, *influxHost, *influxPort, *influxDb)

	if *influxUser != "" && *influxPass != "" {
		url += fmt.Sprintf("&u=%s&p=%s", *influxUser, *influxPass)
	}

	return url
}

func ProcessMetrics(conn net.Conn) {
	var (
		metric   string
		data     []string
		host     string
		instance string
		mType    string
		subType  string
		value    float64
		ts       int
		lastTs   int
		queue    []string
	)

	// this channel is used to properly queue up metrics to be POST'ed to
	// InfluxDB's API
	nextMetric := make(chan string)

	// flushQ will format the metrics payload and POST it to the InfluxDB API
	flushQ := func() {
		if len(queue) == 0 {
			return
		}

		if *debug {
			log.Println("Flushing...", len(queue))
		}

		body := fmt.Sprintf("%s %d", strings.Join(queue, "\n"), lastTs)
		queue = []string{}

		_, err := http.Post(influxUrl, "text/plain", bytes.NewBufferString(body))
		if err != nil {
			log.Println("Failed to POST: " + err.Error())
		}
	}

	// Queue up metrics. When we have a sufficiently large queue of metrics,
	// POST them to the InfluxDB API. Also periodically flush the queue after a
	// certain amount of time.
	go func() {
		timer := time.NewTicker(QUEUE_TIMEOUT)

		for {
			select {
			case m := <-nextMetric:
				queue = append(queue, m)
				if len(queue) >= *maxQueueSize {
					flushQ()
				}
			case <-timer.C:
				flushQ()
			}
		}
	}()

	// Parse the incoming data for metric information. Reformat it to be more
	// friendly to InfluxDB.
	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		metric = scanner.Text()
		data = PAYLOAD_RE.FindStringSubmatch(metric)
		if len(data) == 0 {
			continue
		}

		// extract the bits
		host = data[1]
		instance = data[2]
		mType = data[3]
		subType = data[4]
		value, _ = strconv.ParseFloat(data[5], 0)
		ts, _ = strconv.Atoi(data[6])

		// always flush the queue when we're switching to a different collection interval
		if ts != lastTs && lastTs != 0 {
			flushQ()
		}
		lastTs = ts

		// adapt the metric's tags
		if subType != "" {
			metric = fmt.Sprintf("%s,subType=%s,instance=%s,host=%s value=%f",
				mType, subType, instance, host, value)
		} else {
			metric = fmt.Sprintf("%s,instance=%s,host=%s value=%f",
				mType, instance, host, value)
		}

		// queue this metric
		nextMetric <- metric
	}
}
