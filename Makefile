OUT := graphite-influxdb-proxy

all: build upx

clean:
	rm -f $(OUT)

build:
	go build -ldflags '-s' -o $(OUT)

upx:
	goupx $(OUT)

arch:
	makepkg
